Cryptocurrency Exchange
---------------------------

Add a ethereum based crypto exchange to your drupal site by simply installing
this module and placing block with exchange where you want it to be.
This is module is based on the TOTLE widget and TOTLE API so it automatically 
routes trades to the decentralized exchange that gets you the best prices. 
It also means that it is already integrated with metamask, brave and all the 
rest of the most popular web3 wallets and works great on mobile inside the trust 
wallet app, coinbase wallet, opera, etc…

More info about TOTLE: https://totle.com
See the TOTLE Provacy policy here: https://www.totle.com/totle-privacy-policy
See TOTLE Terms of Use here: https://www.totle.com/totle-terms-of-service
