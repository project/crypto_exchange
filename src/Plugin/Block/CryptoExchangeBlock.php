<?php

namespace Drupal\crypto_exchange\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'CryptoExchangeBlock' block.
 *
 * @Block(
 *  id = "crypto_exchange_block",
 *  admin_label = @Translation("Crypto exchange block"),
 * )
 */
class CryptoExchangeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'crypto_exchange_block';
    $build['#attached'] = ['library' => ['crypto_exchange/totle']];
    $build['crypto_exchange_block']['#markup'] = '<div id="totle-widget"></div>';

    return $build;
  }

}
